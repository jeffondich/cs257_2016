/**
 * WordGameAssistantTest.java
 * Jeff Ondich, 1 April 2016
 */
package edu.carleton.jeffondich;

import org.junit.Assert;
import java.io.FileReader;

public class WordGameAssistantTest {
    private WordGameAssistant wordGameAssistant;

    @org.junit.Before
    public void setUp() throws Exception {
        // We haven't implemented the first constructor for WordGameAssistant
        // yet, but this call will work fine--it just won't do any particular
        // initialization based on the "dictionary.txt" parameter. But we need
        // to instantiate the WordGameAssistant so it can be used in the tests.
        this.wordGameAssistant = new WordGameAssistant("dictionary.txt");

        FileReader reader = new FileReader("dictionary.txt");

        String[] tinyDictionary = {"goat", "gate", "get", "elbow"};
        this.wordGameAssistant = new WordGameAssistant(tinyDictionary);
    }

    @org.junit.After
    public void tearDown() throws Exception {
        // This forces the garbage collector to (eventually) destroy the
        // WordGameAssistant instance we created in setUp.
        this.wordGameAssistant = null;
    }

    @org.junit.Test
    public void toWackyCaseTypical() throws Exception {
        String result = this.wordGameAssistant.toWackyCase("G(OAT!)");
        Assert.assertEquals("Wackification of a typical case failed", "gOaT", result);
    }

    @org.junit.Test
    public void toWackyCaseEmpty() throws Exception {
        String result = this.wordGameAssistant.toWackyCase("");
        Assert.assertEquals("Wackification of the empty string failed", "", result);
    }

    @org.junit.Test
    public void toWackyCaseNull() throws Exception {
        String result = this.wordGameAssistant.toWackyCase(null);
        Assert.assertEquals("Wackification of the null string failed", "", result);
    }

    @org.junit.Test
    public void wordsUsingOnlyLetters() throws Exception {
    }

    @org.junit.Test
    public void wordsUsingLettersAllowingRepetition() throws Exception {
    }

    @org.junit.Test
    public void wordsUsingAllLetters() throws Exception {
    }

    @org.junit.Test
    public void wordsMatchingRegularExpression() throws Exception {
    }

    @org.junit.Test
    public void wordListOrderedBySize() throws Exception {
    }
}
