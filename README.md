# README #

This repository contains sample programs for Jeff Ondich's section of Carleton College's CS257 Software Design course, spring term 2016.

### Notes ###

* The Java samples here are all saved as IntelliJ IDEA projects. At the time of the course, we used IntelliJ IDEA Community Edition 2016.1 and JRE 1.8.

### Contact ###

Jeff Ondich, jondich@carleton.edu